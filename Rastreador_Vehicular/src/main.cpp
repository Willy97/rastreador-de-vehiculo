#include <Arduino.h>

#include "Adafruit_FONA.h"

#define FONA_RX     8
#define FONA_TX     7
#define FONA_RST    4
#define Power       9


char replybuffer[255];
float latitude, longitude, speed_kph, heading, speed_mph, altitude;


#include <SoftwareSerial.h>
SoftwareSerial fonaSS = SoftwareSerial(FONA_TX, FONA_RX);
SoftwareSerial *fonaSerial = &fonaSS;




Adafruit_FONA fona = Adafruit_FONA(FONA_RST);


uint8_t readline(char *buff, uint8_t maxbuff, uint16_t timeout = 0);

void OnOff(){
digitalWrite(9,LOW);
delay(1500);
digitalWrite(9,HIGH);
delay(3000);
Serial.println("Encendido");
}

void setup() {
  
  while (!Serial);

  Serial.begin(9600);

  pinMode(9,OUTPUT);
  digitalWrite(9,HIGH);
  OnOff();
  
  fonaSerial->begin(19200);
  if (! fona.begin(*fonaSerial)) {
    Serial.println(F("Dispositivo no encontrado"));
    while(1);
  }
  Serial.println(F("Listo!"));

  
  char imei[16] = {0}; 
  uint8_t imeiLen = fona.getIMEI(imei);
  if (imeiLen > 0) {
    Serial.print("SIM card IMEI: "); Serial.println(imei);
  }

  fonaSerial->print("AT+CNMI=2,1\r\n");  

  fona.enableGPS(true);

  Serial.println("FONA Listo");
}



  
char fonaNotificationBuffer[64];          
char smsBuffer[250];

void loop() {
  
  char* bufPtr = fonaNotificationBuffer;    
  
  if (fona.available())      
  {
    int slot = 0;           
    int charCount = 0;
  
    do  {
      *bufPtr = fona.read();
      Serial.write(*bufPtr);
      delay(1);
    } while ((*bufPtr++ != '\n') && (fona.available()) && (++charCount < (sizeof(fonaNotificationBuffer)-1)));
    
  
    *bufPtr = 0;

  
    if (1 == sscanf(fonaNotificationBuffer, "+CMTI: " FONA_PREF_SMS_STORAGE ",%d", &slot)) {
      Serial.print("slot: "); Serial.println(slot);
      
      char callerIDbuffer[32]="0973623941";  



      
      if (! fona.getSMSSender(slot, callerIDbuffer, 31)) {
        
      }

      
      Serial.print(F("FROM: ")); Serial.println(callerIDbuffer);

        uint16_t smslen;
        if (fona.readSMS(slot, smsBuffer, 250, &smslen)) { 
          Serial.println(smsBuffer);

        }
boolean gps_success = fona.getGPS(&latitude, &longitude, &speed_kph, &heading, &altitude);
if (gps_success) {
  if(strcmp(smsBuffer,"GPS")==0){
    String LINK=" http://maps.google.com/maps?&z=15&mrt=yp&t=k&q="+String(latitude,6)+"+"+String(longitude,6);
      char link[LINK.length()];
      LINK.toCharArray(link,LINK.length());
    
      
      Serial.println("Enviando respuesta...");
      
      if (!fona.sendSMS(callerIDbuffer,link )) {
        
        Serial.println(F("Fallo!"));
        
      } else {
        Serial.println(F("Enviado!"));
      }
      
    
      if (fona.deleteSMS(slot)) {
        Serial.println(F("OK!"));
      } else {
        fona.deleteSMS(1);
        Serial.print(F("No eliminado ")); Serial.println(slot);
        fona.print(F("AT+CMGD=?\r\n"));
      }
}
  } else {
    Serial.println("Waiting for FONA GPS 3D fix...");
      }
    }
  }
}
